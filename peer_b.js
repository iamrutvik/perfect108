var config = ["127.0.0.1:3001", "127.0.0.1:3003", "127.0.0.1:3005"];
var net = require('net')
var _ = require('lodash')

var server = net.createServer(function (socket) {
  var buffer = '';
  socket.on('data', function (data) {
    parseMessage(data.toString());
  })
})

server.listen(3002)

var discover = () => {
	
	for(i=0;i<=config.length - 1;i++){
		var host = config[i].split(":")[0];
		var port = config[i].split(":")[1];
		var socket = net.connect(port, host);
		socket.on('error', function(err){
		    console.log("Error: "+err.message);
		})
		if(socket){
			var data = '{"type": "expose_config", "sender": "127.0.0.1:3002", "receiver":' + JSON.stringify(config[i]) + '}';
			socket.write(data)
		}
		
	}
	
}
setTimeout(discover, 20000);


var parseMessage = data => {
	var json = JSON.parse(data);;
	switch(json.type){
		case 'expose_config':
			sendConfig(json.sender);
			break;
		case 'config':
			parseConfig(json.config);
			break;
		default:
			break;
	}
}

var sendConfig = receiver => {
	var host = receiver.split(":")[0];
	var port = receiver.split(":")[1];
	var socket = net.connect(port, host);
	socket.on('error', function(err){
	    console.log("Error: "+err.message);
	})
	if(socket){
		var data = '{"type": "config", "sender": "127.0.0.1:3002", "receiver":' + JSON.stringify(receiver) + ', "config" : ' + JSON.stringify(config) + ' }';
		socket.write(data);
	}
}


var parseConfig = config_data => {
	var union = _.union(config, config_data);
	var finalconfig = _.pull(union, "127.0.0.0:3002");
	config = finalconfig;
  	console.log("Config updated: " + JSON.stringify(config));
}