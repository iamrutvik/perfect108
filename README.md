#Prototype - Discovery of peers using multicasting

##Algorithm
Each peer knows the direct links to the other nodes. If we have 6 different noes connected then, a perticular node can be connected to the n random peers. Each Peer willhave config file that will have a direct access endpoint to the neighbour peers.

1. As soon as peer goes live, it will start peer discovery by sending the "expose_config" packet to the directly connected peers by fetching endpoints from config
2. Other connected node will recieve it and parse the packet and acknowledge sender node with config
3. Sender node will parse the acknowledge packet and parse the config and add the new peers to the config file


##How to Run
1. clone the repo using 'git clone https://iamrutvik@bitbucket.org/iamrutvik/perfect108.git'
2. Run 'npm install'. This will install 'lodash' npm module
3. Navigate to the root folder of project
4. Run every peer in saparate terminal. For ex node peer_a.js, node peer_b.js
5. Once done, config file will have a address of each peers
