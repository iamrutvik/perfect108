var config = ["127.0.0.1:3002", "127.0.0.1:3003", "127.0.0.1:3004"];
var net = require('net')
var _ = require('lodash')

//lets create a server that will always listen packets sent fro another nodes
var server = net.createServer(function (socket) {
  var buffer = '';
  socket.on('data', function (data) {
    parseMessage(data.toString());
  })
})

server.listen(3001)

//This method will fetch the directly connected peers and send them a hello packet asking them to expose the config of their
// direclty connect peers 
var discover = () => {
	
	for(i=0;i<=config.length - 1;i++){
		var host = config[i].split(":")[0];
		var port = config[i].split(":")[1];
		var socket = net.connect(port, host);
		socket.on('error', function(err){
		    console.log("Error: "+err.message);
		})
		if(socket){
			var data = '{"type": "expose_config", "sender": "127.0.0.1:3001", "receiver":' + JSON.stringify(config[i]) + '}';
			socket.write(data)
		}
		
	}
	
}

//start peer discovery after 20000 ms once the peer is alive and listening. 
//We can set a interval to check for new nodes every a few ms
setTimeout(discover, 20000);

//Parses a message received by a peer and act according to the packet type.
var parseMessage = data => {
	var json = JSON.parse(data);;
	switch(json.type){
		case 'expose_config':
			sendConfig(json.sender);
			break;
		case 'config':
			parseConfig(json.config);
			break;
		default:
			break;
	}
}

// Connect to the receiver who has requested the config and send it
var sendConfig = receiver => {
	var host = receiver.split(":")[0];
	var port = receiver.split(":")[1];
	var socket = net.connect(port, host);
	socket.on('error', function(err){
	    console.log("Error: "+err.message);
	})
	if(socket){
		var data = '{"type": "config", "sender": "127.0.0.1:3001", "receiver":' + JSON.stringify(receiver) + ', "config" : ' + JSON.stringify(config) + '}';
		socket.write(data);
	}
}

//Receives a config of other nodes, compares and removes duplicates and add new peers to the current config.
var parseConfig = config_data => {
	var union = _.union(config, config_data);
	var finalconfig = _.pull(union, "127.0.0.0:3001");
	config = finalconfig;
  	console.log("Config updated: " + JSON.stringify(config));
}